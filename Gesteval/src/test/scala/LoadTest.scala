import actions._
import com.aqas.loadtest.gesteval.{AuthStorage, ChromeFactory, CsvAccountLoader, EngineFactory, HtmlUnitFactory, ProcessAuth}
import io.gatling.core.Predef._
import io.gatling.http.Predef._

/**
 * Auth, esta simulacíón es usada para incializar todos los tokens de un usuario antes de ejecutar las pruebas de carga,
 * una vez ejecutada esta simulacíón todos los tokens seran almacenados e indexados para cada usuario, para que este
 * proceso no afecte el test de carga de archivos.
 *
 * Se intenta Obtener un token con 3 reintentos maximos por docente.
 */
class LoadTest extends Simulation {
  /**
   * ======================= HTTP PROTOCOL GLOBAL CONFIG=====================
   */
  before({
    val authStorage = AuthStorage.loadInstance();
    val factory = new HtmlUnitFactory;
    val csvAccountLoader = new CsvAccountLoader("Gesteval/src/test/resources/usuarios_gestev_creados.csv")
    val processAuth = new ProcessAuth(csvAccountLoader, "https://srv-si-001.utpl.edu.ec/GEST_EVAL_ERP/", factory,authStorage)

    processAuth.auth()
  })

  val httpProtocol = http
    .baseUrl(ConfigTest.baseUrl)
    .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
    .inferHtmlResources(black = BlackList(""".*\.js""", """.*\.css"""))
    .doNotTrackHeader("1")
    //.inferHtmlResources() // simula el comportamiento del navegador al descargar todo los assets img, videos etc...
    .maxConnectionsPerHostLikeChrome
    .silentResources
    .disableWarmUp
    .acceptLanguageHeader("en-US,en;q=0.5")
    .acceptEncodingHeader("gzip, deflate")
    .userAgentHeader("Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0")
    .maxRedirects(3)
    .disableCaching

  val selectUser = Iterator.continually(UserSimulation.getNextUser().getAsMap())
  val authFlow = scenario("Flow")
    .exec(feed(selectUser))
   // .exec(sessionFunction = (session) => DebugSession(session))
    .exec(CookieSetup())
    .exec(OpenHomeGestEval())
    .exec(CookieSetup())
    .exec(OpenControlAsistencia())
    .exec(CookieSetup())
    .exec(ObtenerAlertas())
    .exec(CookieSetup())
    .exec(ObtenrGruposAsistencia())
    .exec(CookieSetup())
    .exec(Logout())
    //.exec(ActionSendQueryZoom())



  setUp(
    authFlow.inject(
      //atOnceUsers(1)
      //constantUsersPerSec(2) during(5)
      //constantConcurrentUsers(8) during(120)
      rampUsers(100) during (60),
      //rampUsers(16) during (2),
      //rampUsersPerSec(1) to 5 during (new FiniteDuration(25,TimeUnit.SECONDS)),
      )
    ).protocols(httpProtocol)
}

