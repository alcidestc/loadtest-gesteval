package actions

import java.util.concurrent.atomic.AtomicInteger

import com.aqas.loadtest.gesteval.AuthContext
import io.gatling.core.session.Session

/**
 * User, represeta un usuario de pruebas
 *
 * @param username
 * @param password
 * @param cedula
 */
class User (
            val username: String,
            val password:String,
            val cookie:String,
            val context:AuthContext
) {
  private var selected: Boolean = false;
  private var atomicInteger: AtomicInteger = new AtomicInteger(0)

  /**
   * markAsSelected, marca a este usuario como seleccionado en la bateria de pruebas,
   * la restricción es no tomar un usuario repetido para poder hacer seguimiento en la
   * bateria de pruebas
   */
  def markAsSelected(): Unit = {
    atomicInteger.incrementAndGet()
    this.selected = true
  }

  def getNumReferences(): Int = {
    this.atomicInteger.get()
  }

  /**
   * returns boolean
   *
   * @return
   */
  def isSelected(): Boolean = {
    return this.selected;
  }


  /**
   * get Current user as map
   * @return
   */
  def getAsMap():Map[String,Any]={
    var mapa = Map(
      "username"->this.username,
      "password"->this.password,
      "references"->this.getNumReferences(),
      "cookie"->this.cookie
    )
    context.getCookies.forEach(cookie => {
      mapa += (cookie.getName -> cookie.getValue)
      if(cookie.getName.eq(".AspNet.Cookies")){
        mapa += ("aspnet_cookies"-> cookie.getValue)
      }
      if(cookie.getName.eq("ASP.NET_SessionId")){
        mapa += ("aspnet_session"-> cookie.getValue)
      }
    });
    mapa
  }

  def toSession(session:Session):Session={
    var m = session;
    m = m.set("username",username)
    m = m.set("password",password)
    m = m.set("references",getNumReferences())
    m = m.set("cookie",cookie)
    m
  }

}