package actions



import io.gatling.core.Predef._
import io.gatling.core.feeder.FeederBuilder
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._
import org.slf4j.LoggerFactory

import scala.util.Random

object ActionSendQueryZoom {
  val random = new Random()
  def apply() = {
    http("Siette>Enviar Consulta Zoom")
      .post("/Query/Post")
      .header("Content-Type", "application/json; charset=utf-8")
      .header("Accept-Language","es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3")
      .header("UnderTest","${cedula}")
      .body(StringBody("""{"code":"${cedula}"}"""))
      .check(status.is(s => 200))
      .check(regex("SALA DE ZOOM"))
      .check(regex("INFORMACIÓN"))
      .check(regex("${zoomUrl}"))
      //.check(regex("${username}"))
      .check(regex("${dia}"))
      .check(regex("${hora}"))
  }
}
