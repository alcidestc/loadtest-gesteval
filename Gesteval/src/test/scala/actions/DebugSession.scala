package actions



import io.gatling.core.Predef._
import org.slf4j.LoggerFactory

object DebugSession {
  val logger = LoggerFactory.getLogger(DebugSession.getClass)

  def apply(session: Session):Session = {
    val currentUsername:String = session("username").as[String]
    val currentNumReferences:String = session("references").as[String]
    val cookie:String = session("cookie").as[String]
    val aspnetcookie:String = session(".AspNet.Cookies").as[String]
    val aspnetsession:String = session("ASP.NET_SessionId").as[String]
    logger.info("============================ CONTEXT RESULT USER SELECTION =========================")
    logger.info(s"USER [${currentUsername}] REFERENCES [${currentNumReferences}]")
    logger.info(s"""Cookie $cookie""")
    logger.info(s"""ASPNET $aspnetcookie""")
    logger.info(s"""ASPNET Session $aspnetsession""")
    logger.info("========================================================================")
    return session
  }
}
