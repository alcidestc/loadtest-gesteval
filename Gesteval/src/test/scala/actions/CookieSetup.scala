package actions


import io.gatling.core.Predef._
import io.gatling.http.Predef._

import scala.util.Random

object CookieSetup {
  val random = new Random()

  def apply() = {
    addCookie(Cookie(".AspNet.Cookies", "${aspnet_cookies}").withMaxAge(Int.MaxValue).withDomain("/"))
    addCookie(Cookie("ASP.NET_SessionId", "${aspnet_session}").withMaxAge(Int.MaxValue).withDomain("/"))
    addCookie(Cookie("work", "test").withMaxAge(Int.MaxValue).withDomain("/"))
  }
}
