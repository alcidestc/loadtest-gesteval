package actions



import io.gatling.core.Predef._
import io.gatling.core.feeder.FeederBuilder
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._

import scala.util.Random

object OpenHomeGestEval {
  val random = new Random()

  def apply() = {
    http("GESTDEV Abriendo applicacion")
      .get("/")
      .header("Content-Type", "text/html; charset=utf-8")
      .header("Accept-Language","es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3")
      .header("Cookie","${cookie}")
      .header("Set-Cookie","${cookie}")
      //.body(StringBody("""username=${username}&password=${password}"""))
      .check(status.is(s => 200))
  }
}
