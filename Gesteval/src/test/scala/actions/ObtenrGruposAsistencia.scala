package actions



import io.gatling.core.Predef._
import io.gatling.core.feeder.FeederBuilder
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._

import scala.util.Random

object ObtenrGruposAsistencia {
  val random = new Random()

  def apply() = {
    http("GESTDEV Obtener listado Aistencia estudiantes")
      .post("/Asistencia/ObtenerListaAsistenciaControl")
      .header("Content-Type", "application/json; charset=UTF-8")
      .header("Accept-Language","es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3")
      .header("marca_request","AQASDEV")
      .header("Cookie","${cookie}")
      .header("Set-Cookie","${cookie}")
      .body(StringBody("""{asistencia: {"Periodo":"282","TipoEvento":"8272","Centro":null,"Grupo":"SABADO 2020-12-05 08:00-11:00","Fecha":"2020-12-05","TipoAsistencia":"ar_ext"}}"""))
      .check(status.is(s => 200))
      //.check(header("Content-Type").is("application/json; charset=UTF-8"))
      //.check(jsonPath("$.Status").in("0","1"))
  }
}
