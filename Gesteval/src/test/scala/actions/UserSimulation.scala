package actions

import java.util
import java.util.Optional
import java.util.stream.Collectors

import com.typesafe.config.Config
import org.slf4j.LoggerFactory

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import java.text.Normalizer

import com.aqas.loadtest.gesteval.{Account, AuthContext, AuthStorage}

object UserSimulation {
  val logger = LoggerFactory.getLogger(UserSimulation.getClass)

  val setUsers:ArrayBuffer[User] = {
    val accountStorage = AuthStorage.loadInstance()
    var listResult= new ArrayBuffer[User]()
    val accounts = accountStorage.allAccounts()
    accounts.forEach(context => {
      def account = context.getAccount()
      def loggin = account.getLoggin()
      def pwd = account.getPassword()
      def cookies = context.getRawCookie()
      val userRaw = new User(loggin,pwd,cookies,context)
      listResult += userRaw
    })
    listResult
  }
  var counterErroneo = 0;
  val takedUser:ArrayBuffer[User] = new ArrayBuffer[User]()


  def hasAvaibleUsers(): Boolean ={
    return takedUser.size<setUsers.size
  }

  def countUsers(username:String):Int = {
    setUsers.count(puser=>puser.username==username)
  }

  def getNextUser(): User = this.synchronized{
    if(setUsers.isEmpty){
      throw new RuntimeException("No existen usuarios logeados para las preubas")
    }
    val allowReselect:Boolean = ConfigTest.allow_reselects

    var filter:User=>Boolean = null;
    // Si permitir reselecciones esta activiado, no tomar en cuenta el numero de referencias.
    if(allowReselect){
      if(takedUser.size == setUsers.size){
        takedUser.clear()
      }
      //var personMaxNumReferences = setUsers.reduceLeft(this.findMax)
      //println("MAX REFERENCES " + personMaxNumReferences.getNumReferences())
      //filter = (person:User) =>
    }
    filter = (person:User) => !takedUser.contains(person)

    var user = setUsers.filter(filter).map(e=>Optional.ofNullable(e))
      .last

    if(!user.isPresent){
      throw  new IllegalAccessError("Restricted by configuration")
    }

    val resultUser = user.get()
    resultUser.markAsSelected()
    takedUser += resultUser
    resultUser
  }

  // returns the max of the two elements
  def findMax(x: User, y: User): User = {
    val userOne = x.getNumReferences()
    val userTwo = x.getNumReferences()

    val winner = userOne max userTwo
    x
  }


  private val ORIGINAL = "ÁáÉéÍíÓóÚúñÑÜü"
  private val REPLACEMENT = "AaEeIiOoUuNnUu"

  def stripAccents(str: String): String = {
    if (str == null) return null
    val array = str.toCharArray
    for (index <- 0 until array.length) {
      val pos = ORIGINAL.indexOf(array(index))
      if (pos > -1) array(index) = REPLACEMENT.charAt(pos)
    }
    new String(array)
  }

}
