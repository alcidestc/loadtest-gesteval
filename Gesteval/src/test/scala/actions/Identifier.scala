package actions

trait Identifier extends Serializable{
  def get():String
}
