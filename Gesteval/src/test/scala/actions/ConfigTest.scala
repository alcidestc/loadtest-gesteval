package actions

import com.typesafe.config.ConfigFactory

object ConfigTest {

  val config = ConfigFactory.load("simulation").getConfig("local")

  val onlyDefault  =  config.getString("onlyDefault")

  val baseUrl = config.getString("baseUrl")

  val users = config.getConfigList("users")

  // permite seleccionar un usuario nuevamente para pruebas en un estilo circular
  val allow_reselects:Boolean = config.getBoolean("allow_reselects")


  val modalidades = config.getString("modalidad")

  val peekUsers = config.getInt("peek_users")

  val autoGenerateUser = config.getBoolean("autoGenerateUser")

  val generateMaxErrorData = config.getInt("generateMaxErrorData")
}
