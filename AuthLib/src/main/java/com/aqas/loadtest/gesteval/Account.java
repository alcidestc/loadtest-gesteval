package com.aqas.loadtest.gesteval;

import java.io.Serializable;

public interface Account  extends Serializable {
    String getLoggin();
    String getPassword();
}
