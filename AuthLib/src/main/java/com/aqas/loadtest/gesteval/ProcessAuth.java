package com.aqas.loadtest.gesteval;

import me.tongfei.progressbar.ProgressBar;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.*;
import java.util.stream.Collectors;

/**
 * ProcessAuth, proceso de autenticacion usando ADFS
 * @author Ronald Cardenas
 */
public class ProcessAuth {
    private Logger logger = LoggerFactory.getLogger(ProcessAuth.class);
    private final AccountLoader accountLoader;
    private final String urlApplication;
    private final AuthStorage authStorage;
    private final EngineFactory engineFactory;
    private final int MAX_THREADS = Runtime.getRuntime().availableProcessors()*4;;

    public ProcessAuth(AccountLoader accountLoader, String urlApplication,EngineFactory engineFactory, AuthStorage authStorage) {
        this.accountLoader = accountLoader;
        this.urlApplication = urlApplication;
        this.engineFactory = engineFactory;
        this.authStorage = authStorage;
    }
    public ProcessAuth(AccountLoader accountLoader, String urlApplication ) {
       this(accountLoader,urlApplication,new HtmlUnitFactory(),AuthStorage.loadInstance());
    }

    public void auth() {
        // filtra solo las cuentas que requieren logeo
        Set<Account> accounts = accountLoader.load();
//                .stream()
//                .filter((account)-> {
//                    Optional<AuthContext> authContext = authStorage.getAuthForUser(account.getLoggin());
//                    return authContext.map(AuthContext::isExpired).orElse(true);
//                }).collect(Collectors.toSet());

        ExecutorService executorService = Executors.newScheduledThreadPool(2);
        ProgressBar progressBar = new ProgressBar("UTPL ADFS", accounts.size());
        System.out.println("=========================AQAS ADFS loggin=========================");
        System.out.println(String.format("Logeando todos los usuarios"));
        System.out.println(String.format("THREADS [%d] Tareas",MAX_THREADS));
        System.out.println(String.format("DRIVER FACTORY [%s]",engineFactory.getClass().getName()));
        System.out.println(String.format("Total de cuentas [%d]",accounts.size()));
        System.out.println("==================================================================");
        List<? extends Future<?>> tasks = accounts.stream()
                .map(account -> executorService.submit(() -> {
                    String user = account.getLoggin();
                    String pwd = account.getPassword();
                    try {
                        WebDriver webDriver = engineFactory.create();
                        logger.trace(String.format("User [%s] Pwd [%s] using thread [%s]", user, pwd, Thread.currentThread()));
                        AuthStragy authStragy = new DefaultAuthStragy(urlApplication, webDriver, account);
                        AuthContext authContext = authStragy.loggin();
                        authStorage.saveAuthForUser(authContext);
                        AuthStorage.serializeAndSave(authStorage);
                        webDriver.close();
                        webDriver.quit();
                        progressBar.step();
                    } catch (Exception exception) {
                        progressBar.setExtraMessage(String.format("[%s] ERR",user));
                        exception.printStackTrace();
                    }
                })).collect(Collectors.toList());
        try {
            //executorService.awaitTermination(5, TimeUnit.MINUTES);
            tasks.forEach(this::waitFutureResult);
            System.out.println("Marcando executor service para finalizar cuando las tareas terminen");
            executorService.shutdown();
            progressBar.close();
        } catch (Exception exception) {
            exception.printStackTrace();
        }

    }

    private void waitFutureResult(Future<?> future) {
        try {
            future.get(3000,TimeUnit.SECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            e.printStackTrace();
        }
    }

    void processLogout() {
        Set<Account> accounts = accountLoader.load();
        ExecutorService executorService = Executors.newFixedThreadPool(8);
        accounts.stream()
                .map(account -> executorService.submit(() -> {
                    String user = account.getLoggin();
                    String pwd = account.getPassword();
                    try {

                        WebDriver webDriver = engineFactory.create();
                        logger.trace(String.format("User [%s] Pwd [%s] using thread [%s]", user, pwd, Thread.currentThread()));
                        Optional<AuthContext> result1 = authStorage.getAuthForUser(user);
                        if (result1.isPresent()) {
                            AuthStragy authStragy = new DefaultAuthStragy(urlApplication, webDriver, account);
                            authStragy.logout(result1.get());
                            AuthStorage.serializeAndSave(authStorage);
                            webDriver.close();
                            webDriver.quit();
                        } else {
                            // me da pereza configurar un logger se va con fprint
                            logger.debug(String.format("No existe datos de Auth para el usuaio [%s]", user));
                        }
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }
                })).collect(Collectors.toList());
        try {
            executorService.awaitTermination(1, TimeUnit.MINUTES);
        } catch (Exception exception) {
            exception.printStackTrace();
        }

    }

    public static void main(String[] args) {
        AuthStorage authStorage =  AuthStorage.loadInstance();
        EngineFactory factory = new ChromeFactory();
        CsvAccountLoader csvAccountLoader = new CsvAccountLoader("C:\\Users\\ronald\\workenv-aqas\\load-test-zoomquery\\Gesteval\\src\\test\\resources\\usuarios_gestev_creados.csv");
        ProcessAuth processAuth = new ProcessAuth(csvAccountLoader, "https://srv-si-001.utpl.edu.ec/GEST_EVAL_ERP/",factory,authStorage);
        processAuth.auth();
    }
}
