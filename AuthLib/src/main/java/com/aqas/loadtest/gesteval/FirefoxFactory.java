package com.aqas.loadtest.gesteval;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

public class FirefoxFactory implements EngineFactory{
    private static final String PATH_FIREFOX_DRIVER_LINUX = "src/test/resources/drivers/geckodriver";
    private static final String PATH_FIREFOX_DRIVER_WIN = "src/test/resources/drivers/geckodriver.exe";

    private String driver;
    /**
     * constructor usando como argumento el path del driver a utilizr
     * @param driver
     */
    FirefoxFactory(String driver){
        this.driver = driver;
        System.setProperty("webdriver.gecko.driver", driver);
    }

    FirefoxFactory(){
        if(OsUtils.isUnix()){
            this.driver = PATH_FIREFOX_DRIVER_LINUX;
        }else if(OsUtils.isWindows()){
            this.driver = PATH_FIREFOX_DRIVER_WIN;

        }else{
            throw new IllegalStateException("No soportado contacar a ronald :v");
        }
    }


    /**
     * Factory method
     *
     * @return
     */
    @Override
    public WebDriver create() {
        System.setProperty("webdriver.gecko.driver", driver);
        FirefoxOptions firefoxOptions = new FirefoxOptions();
        WebDriver webDriver = new org.openqa.selenium.firefox.FirefoxDriver();
        return webDriver;
    }
}
