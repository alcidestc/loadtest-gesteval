package com.aqas.loadtest.gesteval;

import org.openqa.selenium.WebDriver;

/**
 * interface encargada de contruir el engine para el proceso
 * chrome
 * firefox
 */
public interface EngineFactory {
    /**
     * Factory method
     * @return
     */
    WebDriver create();
}
