package com.aqas.loadtest.gesteval;

import java.util.Set;

public interface AccountLoader {
    Set<Account> load();

}
