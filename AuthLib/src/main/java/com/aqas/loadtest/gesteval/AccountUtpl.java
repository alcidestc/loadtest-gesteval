package com.aqas.loadtest.gesteval;

import java.io.Serializable;

public class AccountUtpl implements Account, Serializable {
    private static final long serialVersionUID = 8329685098267757690L;

    private final String correo;
    private final String passowrd;

    public AccountUtpl(String correo, String passowrd) {
        this.correo = correo + "@utpl.edu.ec";
        this.passowrd = passowrd;
    }

    @Override
    public String getLoggin() {
        return correo;
    }

    @Override
    public String getPassword() {
        return passowrd;
    }

    @Override
    public String toString() {
        return "AccountUtpl{" +
                "correo='" + correo + '\'' +
                ", passowrd='" + passowrd + '\'' +
                '}';
    }
}
