package com.aqas.loadtest.gesteval;

import java.io.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class AuthStorage implements Serializable {
    public static final String FILE_NAME_STORAGE = "TOKENS_SERIAL_CACHE";
    private static final long serialVersionUID = 1529685098267757690L;
    private Map<String, AuthContext> authContextMap = new ConcurrentHashMap<>();
    private static AuthStorage instance = null;

    /**
     * conmstructo priuvado solo puede ser contruido usando static factory method AuthMemory.loadInstance()
     */
    private AuthStorage(){}


    public void deleteAuth(String username){
        this.authContextMap.remove(username);
    }

    public synchronized void saveAuthForUser(AuthContext authContext) {
        Account account = authContext.getAccount();
        this.authContextMap.put(account.getLoggin(), authContext);
    }

    public List<AuthContext> allAccounts(){
        List<AuthContext> values  = authContextMap.entrySet().stream().map((stringAuthContextEntry -> stringAuthContextEntry.getValue()))
                .collect(Collectors.toList());
        return values;
    }

    public Optional<AuthContext> getAuthForUser(String user) {
        return Optional.ofNullable(authContextMap.get(user));
    }

    public static synchronized void serializeAndSave(AuthStorage authStorage) {
        ObjectOutputStream oos = null;
        try {
            oos = new ObjectOutputStream(new FileOutputStream(FILE_NAME_STORAGE));
            oos.writeObject(authStorage);
            oos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * crea o cara una nueva instancia de la clase
     * @return
     */
    public static synchronized AuthStorage loadInstance(String filename) {
        if (instance == null) {
            File file = new File(filename);
            if (!file.exists()) {
                System.err.println("Fichero no encontrado");
                instance = new AuthStorage();
            } else {
                try {
                    ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
                    instance = (AuthStorage) ois.readObject();
                    ois.close();
                } catch (IOException | ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
        return instance;
    }

    public static synchronized AuthStorage loadInstance() {
        return loadInstance(FILE_NAME_STORAGE);
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AuthStorage that = (AuthStorage) o;
        return Objects.equals(authContextMap, that.authContextMap);
    }

    @Override
    public int hashCode() {
        return Objects.hash(authContextMap);
    }

    @Override
    public String
    toString() {
        return "AuthMemory{" +
                "authContextMap=" + authContextMap +
                '}';
    }
}
