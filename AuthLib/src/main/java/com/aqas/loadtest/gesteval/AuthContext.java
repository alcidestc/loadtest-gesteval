package com.aqas.loadtest.gesteval;

import org.openqa.selenium.Cookie;

import java.io.Serializable;
import java.util.*;

/**
 * Cookie, esta clase es un value object y representa el valor raw de una cookie obtenida al logearse
 */
public class AuthContext implements Serializable {
    private static final long serialVersionUID = 2529685098267757690L;
    private Set<org.openqa.selenium.Cookie> cookies;
    private String rawCookie;
    private final Account account;
    private Date createAt;
    private Date expireAt;
    public AuthContext(Account account, Set<Cookie> cookieSet) {
        this.account = account;
        this.cookies = cookieSet;
        this.createAt = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(createAt);
        cal.add(Calendar.MINUTE, 20);
        this.expireAt = cal.getTime();

//        for (Cookie cookie : cookieSet) {
//            System.out.println("-----------------------");
//            System.out.println(cookie.getName());
//            System.out.println(cookie);
//            System.out.println(cookie.getExpiry());
//            System.out.println(cookie.getPath());
//            System.out.println(cookie.isSecure());
//            System.out.println(cookie.isHttpOnly());
//            System.out.println(cookie.toJson());
//        }
    }

    public Account getAccount() {
        return account;
    }

    public Set<Cookie> getCookies() {
        return cookies;
    }

    public boolean isExpired(){
        Date cuurentTime = new Date();
        int value =  expireAt.compareTo(cuurentTime);
        return value<=0;
    }

    public String getRawCookie() {
        if (rawCookie == null) {
            StringBuilder cookieBuilder = new StringBuilder();
            StringJoiner cookieJoiner = new StringJoiner(";");
            //cookieBuilder.append("Cookie: ");
            for (org.openqa.selenium.Cookie cookie : cookies) {
                cookieJoiner.add(cookie.getName() + "=" + cookie.getValue());
            }
            cookieBuilder.append(cookieJoiner.toString());
            rawCookie = cookieBuilder.toString();
        }
        return rawCookie;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AuthContext that = (AuthContext) o;
        return Objects.equals(cookies, that.cookies) &&
                Objects.equals(rawCookie, that.rawCookie) &&
                Objects.equals(account, that.account);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cookies, rawCookie, account);
    }

    @Override
    public String toString() {
        return "AuthContext{" +
                "cookies=" + cookies +
                ", rawCookie='" + rawCookie + '\'' +
                ", account=" + account +
                '}';
    }
}
