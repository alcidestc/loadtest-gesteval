package com.aqas.loadtest.gesteval;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class ChromeFactory implements EngineFactory{
    private static final String PATH_CHROME_DRIVER_LINUX = "src/test/resources/drivers/chromedriver";
    private static final String PATH_CHROME_DRIVER_WIN = "src/test/resources/drivers/chromedriver.exe";

    private String driver;
    /**
     * constructor usando como argumento el path del driver a utilizr
     * @param driver
     */
    ChromeFactory(String driver){
        this.driver = driver;
        System.setProperty("webdriver.gecko.driver", driver);
    }

    ChromeFactory(){
        if(OsUtils.isUnix()){
            this.driver = PATH_CHROME_DRIVER_LINUX;
        }else if(OsUtils.isWindows()){
            this.driver = PATH_CHROME_DRIVER_WIN;

        }else{
            throw new IllegalStateException("No soportado contacar a ronald :v");
        }
    }


    /**
     * Factory method
     *
     * @return
     */
    @Override
    public WebDriver create() {
        System.setProperty("webdriver.chrome.driver", driver);
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--headless");
        //options.addArguments("user-data-dir=C:\\Users\\ronald\\AppData\\Local\\Google\\Chrome\\User Data\\Default");
        WebDriver webDriver = new org.openqa.selenium.chrome.ChromeDriver(options);
        return webDriver;
    }
}
