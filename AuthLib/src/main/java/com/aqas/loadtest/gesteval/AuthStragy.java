package com.aqas.loadtest.gesteval;

import org.openqa.selenium.WebDriver;

public interface AuthStragy {
    /**
     * loggin, ejecuta el proceso de autenticacion y retorna los datos generado
     * luego del proceso para ser reutilizados por gatling
     * @return
     */
    AuthContext loggin();


    void logout(AuthContext authContext);
}
