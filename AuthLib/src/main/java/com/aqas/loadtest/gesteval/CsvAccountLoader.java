package com.aqas.loadtest.gesteval;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.HashSet;
import java.util.Set;

public class CsvAccountLoader implements AccountLoader {
    private String[] HEADERS = { "NombresCompletos","Usuario", "Contrasena"};
    private final String file;
    private Set<Account> accounts = new HashSet<>();
    private char delimter;

    public CsvAccountLoader(String file, char delimter) {
        this.file = file;
        this.delimter = delimter;
    }

    public CsvAccountLoader(String file) {
        this.file = file;
        this.delimter = ';';
    }

    /**
     * Carga las cuentas de los usuarios
     * @return
     */
    @Override
    public Set<Account> load() {
        try {
            Reader in = new FileReader(file);
            Iterable<CSVRecord> records = CSVFormat.DEFAULT
                    .withHeader(HEADERS)
                    .withDelimiter(';')
                    .withFirstRecordAsHeader()
                    .parse(in);
            for (CSVRecord record : records) {
                String user = record.get("Usuario");
                String pwd = record.get("Contrasena");
                Account account = new AccountUtpl(user, pwd);
                accounts.add(account);
            }
        } catch (IOException e) {
            throw new AuthException("Imposible cargar datos desde " + file,e);
        }
        return accounts;
    }

    public static void main(String[] args) {
        AccountLoader accountLoader = new CsvAccountLoader("C:\\Users\\ronald\\workenv-aqas\\load-test-zoomquery\\Gesteval\\src\\test\\resources\\usuarios_gestev_creados.csv");
        Set<Account> datos = accountLoader.load();
        System.out.println(datos);

    }
}
