package com.aqas.loadtest.gesteval;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
/*
import java.net.*;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
*/
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class DefaultAuthStragy implements AuthStragy {

    private final String application;
    private final WebDriver webDriver;
    private final Account account;
    private final String username;
    private final String password;
    private final String rol;

    public DefaultAuthStragy(String application, WebDriver webDriver, Account account, String rol) {
        this.application = application;
        this.account = account;
        this.webDriver = webDriver;
        this.username = account.getLoggin();
        this.password = account.getPassword();
        this.rol = rol;
    }

    public DefaultAuthStragy(String application, WebDriver webDriver, Account account) {
        this(application, webDriver, account, "COORDINADOR");
    }

    @Override
    public AuthContext loggin() {
        webDriver.get(application);
        WebElement userinpunt = webDriver.findElement(By.id("userNameInput"));
        WebElement passworin = webDriver.findElement(By.id("passwordInput"));
        WebElement submit = webDriver.findElement(By.id("submitButton"));
        userinpunt.sendKeys(username);
        passworin.sendKeys(password);
        submit.click();
        waitForAuth();
        selectRol();
        Set<org.openqa.selenium.Cookie> cookies = webDriver.manage().getCookies();
        AuthContext authContext = new AuthContext(account, cookies);
        return authContext;
    }

    @Override
    public void logout(AuthContext authContext) {
        webDriver.get("https://srv-si-001.utpl.edu.ec/GEST_EVAL_ERP/");
        for (Cookie cookie : authContext.getCookies()) {
            webDriver.manage().addCookie(cookie);
        }
        waitForAuth();
        String roles = String.format("https://srv-si-001.utpl.edu.ec/GEST_EVAL_ERP/Home/Principal?login=%s&rol=GES_CONFIG_EVAL_COORDINADOR", username);
        webDriver.get(roles);
        webDriver.get("https://srv-si-001.utpl.edu.ec/GEST_EVAL_ERP/Account/SignOut");
    }

    public void waitForAuth() {
        WebDriverWait wait = new WebDriverWait(webDriver, 20);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("btnIngresoRol")));
        wait.until(ExpectedConditions.presenceOfNestedElementsLocatedBy(By.id("rol_ingreso"), By.tagName("option")));
    }

    public void selectRol()  {
        BasicCookieStore cookieStore = new BasicCookieStore();
        for (Cookie cookieSelenium : webDriver.manage().getCookies()) {
            BasicClientCookie cookie = new BasicClientCookie(cookieSelenium.getName(), cookieSelenium.getValue());
            cookie.setDomain(cookieSelenium.getDomain());
            cookie.setPath(cookieSelenium.getPath());
            cookieStore.addCookie(cookie);
        }

        try {
            CloseableHttpClient apacheClient = HttpClientBuilder.create().setDefaultCookieStore(cookieStore).build();
            final HttpPost requestPost = new HttpPost("https://srv-si-001.utpl.edu.ec/GEST_EVAL_ERP/Utilities/GetRolesDB");
            requestPost.addHeader("Content-Type", "application/x-www-form-urlencoded");
            List<NameValuePair> nvps = new ArrayList<NameValuePair>();
            nvps.add(new BasicNameValuePair("login", username));
            requestPost.setEntity(new UrlEncodedFormEntity(nvps, HTTP.UTF_8));
            CloseableHttpResponse response = apacheClient.execute(requestPost);
            String responseBody = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8);
            JSONObject jsonObject = new JSONObject(responseBody);
            JSONArray roles = jsonObject.getJSONArray("Data");
            String rolecode = "";
            if (roles.length() > 0) {
                JSONObject rol = roles.getJSONObject(0);
                rolecode = rol.getString("RLU_NOMBRE");
                String urlRol = String.format("https://srv-si-001.utpl.edu.ec/GEST_EVAL_ERP/Home/Principal?login=%s&rol=%s", username, rolecode);
                webDriver.get(urlRol);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public static void main(String[] args) throws InterruptedException {
        EngineFactory factory = new ChromeFactory();
        WebDriver driver = factory.create();
        Account account = new AccountUtpl("pru_gestev01@utpl.edu.ec", "Gest3v.01");
        DefaultAuthStragy defaultAuthStragy = new DefaultAuthStragy("https://srv-si-001.utpl.edu.ec/GEST_EVAL_ERP/", driver, account);
        AuthContext authContext = defaultAuthStragy.loggin();
        System.out.println(authContext.getRawCookie());
    }
}
