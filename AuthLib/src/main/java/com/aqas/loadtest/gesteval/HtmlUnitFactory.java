package com.aqas.loadtest.gesteval;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.Cache;
import com.gargoylesoftware.htmlunit.WebClient;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import java.util.concurrent.CountDownLatch;
import java.util.logging.Level;

public class HtmlUnitFactory implements EngineFactory{
    private static final CountDownLatch countDownLatch = new CountDownLatch(1);
    private String driver;
    private Cache cache;

    public HtmlUnitFactory(){
    }


    /**
     * Factory method
     *
     * @return
     */
    @Override
    public WebDriver create() {
        java.util.logging.Logger.getLogger("com.gargoylesoftware.htmlunit").setLevel(Level.OFF);
        WebDriver webDriver = new HtmlUnitDriver(BrowserVersion.FIREFOX,true){
            @Override
            protected WebClient modifyWebClient(WebClient client) {
                final WebClient webClient = super.modifyWebClient(client);
                webClient.getOptions().setCssEnabled(false);
                webClient.getOptions().setJavaScriptEnabled(true);
                webClient.getOptions().setDownloadImages(false);
//                if(countDownLatch.getCount()>=1){
//                    // reusando cache para todas las instancias del browser
//                    cache = webClient.getCache();
//                    countDownLatch.countDown();
//                }else{
//                    webClient.setCache(cache);
//                }
                return webClient;
            }
        };
        return webDriver;
    }
}
